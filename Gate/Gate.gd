extends Node2D

export (PackedScene) var IndividualScene
export (PackedScene) var WantedSignScene

var character_number = 4
var unwanted_count = 100
var wanted_number = 3
var wanted_individuals = []

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$YouWin.visible = false
	# spawn
	for i in range(unwanted_count):
		spawn_individual()
	# randomize wanted_individuals
	wanted_individuals = []
	for i in range(wanted_number):
		var char_config = null
		while char_config == null or char_config in wanted_individuals:
			char_config = [
				1 + randi() % character_number, 
				1 + randi() % character_number, 
				1 + randi() % character_number
				]
		wanted_individuals.append(char_config)
	#
	for char_config_i in wanted_individuals:
		spawn_individual(char_config_i, true)
		put_individual_on_display(char_config_i)

func put_individual_on_display(char_config):
	#
	var wanted_sign = WantedSignScene.instance()
	# load sprites
	wanted_sign.get_node("Individual").load_char_config(char_config)
	# set display
	wanted_sign.get_node("Individual").display_individual()
	#
	$WantedSigns.add_child(wanted_sign)

func spawn_individual(char_config = null, wanted = false):
	# reroll character set until it is not in wanted list
	if char_config == null:
		while char_config == null or char_config in wanted_individuals:
			char_config = [
				1 + randi() % character_number, 
				1 + randi() % character_number, 
				1 + randi() % character_number
				]
	#
	var individual = IndividualScene.instance()
	# load sprites
	individual.load_char_config(char_config)
	# connect to respawn function
	individual.connect("limit_reached", self, "_on_limit_reached")
	# x spawn position
	var x_spawn = 200+randi() % 350
	# put in wanted if wanted
	if wanted:
		$Wanted.add_child(individual)
		# random start position
		if randi()%2:
			individual.start(Vector2(-800 -randi()%500, x_spawn), false)
		else:
			individual.start(Vector2(1300+randi()%500, x_spawn), true)
	else:
		$Individuals.add_child(individual)
		# random start position
		if randi()%2:
			individual.start(Vector2(-50 -randi()%1000, x_spawn), false)
		else:
			individual.start(Vector2(1050+randi()%1000, x_spawn), true)

func _on_limit_reached(char_config):
	# respawn when leaving the map
	if char_config in wanted_individuals:
		spawn_individual(char_config, true)
	else:
		spawn_individual()

func _input(event):
	# Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		# on left mouseclick
		if event.button_index == 1:
			# only if timer is up
			if $ClickSpamTimer.is_stopped():
				$ClickSpamTimer.start()
				var space_state = get_world_2d().direct_space_state
				# check for areas on clicked point
				for object in space_state.intersect_point(event.position, 32, [  ], 0x7FFFFFFF, false, true):
					# delete clicked individual if wanted
					if object.collider.get_parent().get_parent().name == "Wanted":
						print("Cought one!")
						object.collider.get_parent().queue_free()
						var uncrossed_count = 0
						for wanted_sign in $WantedSigns.get_children():
							if wanted_sign.get_node("Individual").char_config == object.collider.get_parent().char_config:
								wanted_sign.get_node("RedCross").visible = true
							if wanted_sign.get_node("RedCross").visible == false:
								uncrossed_count += 1
						if uncrossed_count == 0:
							$YouWin.visible = true 
							yield(get_tree().create_timer(3), "timeout")
							get_tree().reload_current_scene()
