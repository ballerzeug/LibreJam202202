extends Control

export (PackedScene) var GateScene

func _on_Start_pressed():
	get_tree().change_scene_to(GateScene)
