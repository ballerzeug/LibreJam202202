extends KinematicBody2D

export (bool) var left_orientation = false

signal limit_reached

var orientation_factor = 1
var ray_length = 100
var walk_speed = 50
var default_playback_speed = 1.0
var state = null
var forward_direction = Vector2(1,0) 
var last_pos = Vector2(0,0)
var char_config = ["template", "template", "template"]

enum {DISPLAY, WALKING}

func _ready():
	# random walkspeed
	__set_walk_speed(30 + randi()%30)

func __set_walk_speed(new_walk_speed):
	walk_speed = new_walk_speed
	default_playback_speed = walk_speed / 50.0

func start(pos, _left_orientation):
	#
	set_position(pos) 
	# change direction
	__change_direction(_left_orientation)
	#
	change_state(WALKING)
	
func __change_direction(_left_orientation):
	left_orientation = _left_orientation
	if left_orientation:
		orientation_factor = -1
	scale.x *= orientation_factor
	forward_direction *= orientation_factor
	
func display_individual(pos=null):
	#
	if pos != null:
		set_position(pos)
	#
	change_state(DISPLAY)
	
func load_char_config(new_char_config):
	char_config = new_char_config
	$Sprites/Body.set_texture(load(
		"res://Assets/Character_" + String(char_config[0]) + "/Body.png"))
	$Sprites/Body/Arm1.set_texture(load(
		"res://Assets/Character_" + String(char_config[1]) + "/Arm1.png"))
	$Sprites/Body/Arm2.set_texture(load(
		"res://Assets/Character_" + String(char_config[1]) + "/Arm2.png"))
	$Sprites/Body/UpperLeg1.set_texture(load(
		"res://Assets/Character_" + String(char_config[2]) + "/UpperLeg1.png"))
	$Sprites/Body/UpperLeg2.set_texture(load(
		"res://Assets/Character_" + String(char_config[2]) + "/UpperLeg2.png"))
	$Sprites/Body/UpperLeg1/LowerLeg1.set_texture(load(
		"res://Assets/Character_" + String(char_config[2]) + "/LowerLeg1.png"))
	$Sprites/Body/UpperLeg2/LowerLeg2.set_texture(load(
		"res://Assets/Character_" + String(char_config[2]) + "/LowerLeg2.png"))

func __y_dif_change(result):
	return orientation_factor * (1-abs(global_position.x - result.position.x) / ray_length)

func _physics_process(delta):
		#
	match state:
		WALKING:
			var space_state = get_world_2d().direct_space_state
			# look for colliders in front
			var up2_result = space_state.intersect_ray(
				global_position + Vector2(0, 1.5*$CollisionShape2D.shape.radius), 
				global_position + Vector2(orientation_factor*ray_length, 1.5*$CollisionShape2D.shape.radius), 
				[self])
			var up1_result = space_state.intersect_ray(
				global_position + Vector2(0, 0.9*$CollisionShape2D.shape.radius), 
				global_position + Vector2(orientation_factor*ray_length, 0.9*$CollisionShape2D.shape.radius), 
				[self])
			var down1_result = space_state.intersect_ray(
				global_position + Vector2(0, -1*$CollisionShape2D.shape.radius), 
				global_position + Vector2(orientation_factor*ray_length, -1*$CollisionShape2D.shape.radius), 
				[self])
			var down2_result = space_state.intersect_ray(
				global_position + Vector2(0, -1.5*$CollisionShape2D.shape.radius), 
				global_position + Vector2(orientation_factor*ray_length, -1.5*$CollisionShape2D.shape.radius), 
				[self])
			# nearer colliders lead to stronger reaction 
			var y_dif = 0.0
			if not up2_result.empty():
				y_dif -= 2 * __y_dif_change(up2_result)
			if not up1_result.empty():
				y_dif -= 1.5 * __y_dif_change(up1_result)
			if not down1_result.empty():
				y_dif += 1 * __y_dif_change(down1_result)
			if not down2_result.empty():
				y_dif += 2 * __y_dif_change(down2_result)
			# try to avoid walls
			if position.y < 200:
				y_dif += orientation_factor * 0.5 
			if position.y > 550:
				y_dif -= orientation_factor * 0.5
			#
			var motion = forward_direction.rotated(tanh(y_dif)) * walk_speed * delta
			# move
			move_and_collide(motion)
			#
			var real_speed = (position - last_pos).length()
			last_pos = position
			if delta != 0:
				$AnimationPlayer.playback_speed = default_playback_speed * real_speed / (walk_speed * delta)
			#
			if (position.x < -200 and left_orientation) or (position.x > 1200 and not left_orientation):
				emit_signal("limit_reached", char_config)
				queue_free()
	# change z_index according to y position, times 10 because sprites have +-3 range
	z_index = global_position.y

func change_state(new_state):
	match new_state:
		DISPLAY:
			$CollisionShape2D.set_deferred("disabled", true)
			$AnimationPlayer.play("RESET")
		WALKING:
			last_pos = position
			$CollisionShape2D.set_deferred("disabled", false)
			$AnimationPlayer.play("walk")
	state = new_state
